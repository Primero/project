import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.8
import QtGraphicalEffects 1.0

ApplicationWindow {

    signal sendAuth(string login, string password)
    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")

    SwipeView {  // компонент для пролистывания страниц
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page{
            ColumnLayout {
                clip: false
                opacity: 0.6
                anchors.leftMargin: 0
                anchors.fill: parent;
                Rectangle {
                    Layout.preferredWidth:  70 * Screen.pixelDensity
                    Layout.preferredHeight: 40 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignCenter
                    ColumnLayout {
                        anchors.centerIn: parent;
                        width:50 * Screen.pixelDensity
                        height:50 * Screen.pixelDensity
                        Rectangle {
                            color: "white"
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: edtLogin1
                                font.pointSize: 8
                                anchors.fill: parent;
                                placeholderText: "Enter your login"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }
                        Rectangle {
                            color: "white"
                            border.width: 1
                            border.color: "black"
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            TextField {
                                id: edtPassword1
                                font.pointSize: 8
                                anchors.fill: parent;
                                placeholderText: "Enter your password"
                                //при размещении в лэйоут элементов,  мы не используем width и x,y
                            }
                        }


                        DropShadow {
                            anchors.fill: btnAuth
                            horizontalOffset: 3
                            verticalOffset: 3
                            radius: 8.0
                            samples: 17
                            color: "#80000000"
                            source: btnAuth
                        }
                        Button {
                            id: btnAuth
                            text: "Вход"
                            font.pointSize: 18
                            Layout.preferredWidth:  40 * Screen.pixelDensity
                            Layout.preferredHeight: 10 * Screen.pixelDensity
                            Layout.alignment: Qt.AlignCenter
                            onClicked: {sendAuth(edtLogin1.text, edtPassword1.text);

                            }


                        }




                        Item {
                            Layout.fillHeight: true}

                    }
                }
            }
        }

        Page {
            id: vkFriends
            Rectangle {
                id: backgroundfriends
                anchors.fill: parent
                color: "yellow"
            }
            Item {
                id: item1
                anchors.fill: parent
                   ListView {
                       anchors.fill: parent
                       model: modelFriends
                       spacing: 20
                       delegate: Rectangle {
                               id: rec1
                               color: "white"
                               height: 120
                               width: parent.width
                               radius: 10
                               anchors.margins: 20
                               opacity: 0.8

                               GridLayout {
                                   anchors.fill: parent
                                   opacity: 1
                                   Image {
                                       id: avatar
                                       source: photo
                                       anchors.right: parent.right
                                       anchors.top: parent.top
                                       anchors.rightMargin: 25
                                       anchors.topMargin: 10
                                       Layout.row: 1
                                       Layout.column: 2
                                       Layout.rowSpan: 4
                                   }

                                   Text {
                                       id: id1
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 1
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "ID friend - " + friendid
                                   }
                                   Text {
                                       id: id2
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 2
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Name friend: " + friendname
                                   }

                                   Text {
                                       id: id3
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 3
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "Status:"
                                   }
                                   Text {
                                       id: id4
                                       font.pointSize: 8
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 4
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: "\"" + status + "\""
                                   }
                               }
                           }
                   }
            }
        }

        Page{
        ListView{ anchors.fill: parent}
        }

        Page { //мультимедия
            MediaPlayer{
                //класс для открытия и обработки аудио/видеофайлов
                id: mediaplayer
                autoLoad: true //грузиться файл сразу, без команды
                loops: MediaPlayer.Infinite //число повторений
                autoPlay: true

                source: "file:///C:/Users/MobileDeveloper/Documents/galaganovswipe/sample.avi"
                //source: "sample.avi"



            }
            Camera{id: camera
            }

            ColumnLayout{
                anchors.fill: parent
                VideoOutput{
                    enabled: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    //anchors.file: parent
                    source: mediaplayer

                }
                /*Button { */

            }
        }
        Page { //мультимедия
            MediaPlayer{
                //класс для открытия и обработки аудио/видеофайлов
                //id: camera
                autoLoad: true //грузиться файл сразу, без команды
                loops: MediaPlayer.Infinite //число повторений
                autoPlay: true

                //source: "file:///C:/Users/MobileDeveloper/Documents/galaganovswipe/sample.avi"
                //source: "sample.avi"



            }
            // Camera{id: camera }

            ColumnLayout{
                anchors.fill: parent
                Button{
                    text: "Photo"
                    font.pointSize: 18
                    onClicked: {camera.imageCapture.capture()}}
                Button{
                    text: "Video"
                    font.pointSize: 18
                    onClicked: {camera.videoRecorder.record()}}
                Button{
                    text: "Stop Video"
                    font.pointSize: 18
                    onClicked: {camera.videoRecorder.stop()}}
                VideoOutput{
                    enabled: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    //anchors.file: parent
                    //   source: camera

                }
                /*Button { */

            }
        }


    }


}
