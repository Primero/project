#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "authcontroller.h"
#include "cryptocontroller.h"
#include <QQmlContext>
#include "db_controller.h"
#include "messagecontroller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    AuthController authController; // слот появляется здесь
    Cryptocontroller cryptocontroller;

  cryptocontroller.encryptFile("f0.txt", "123456789", "123456789");
 // cryptocontroller.DencryptFile("f1.txt", "123456789", "987654321");



     db_controller database;
     database.sendQuieries();




    QQmlApplicationEngine engine;
    QQmlContext * ctxt = engine.rootContext();

    ctxt->setContextProperty("modelFriends", &(authController.friendsModel));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject * appWindow = engine.rootObjects().first();



    QObject::connect(appWindow,
                     SIGNAL(sendAuth(QString, QString)), // чей и какой сигнал
                     &authController,
                     SLOT(Authentificate(QString,QString))); // к чьему и какому слоту

    return app.exec();
}


/* Библиотеки (*.cpp , *.h)
//1. Исходный код.
+ простота подключения
+ меньше ограничений по платформам
- подходит только для малых объемов кода.
- при запуске программы, такие библиотеки перестраиваются каждый раз
- В каждом приложении своя копия
- Объем
- Невозможно обновить версию библиотеки, обновив версию приложения
2. Статическое подключение.
Один файл *.lib(win) *.a(linux)
+ Строится отдельньо от программы в отдельном проекте
- Сложность подключения
- Бинарная несовместимость
- В каждом приложении своя копия
- Объем
- Невозможно обновить версию библиотеки, обновив версию приложения
3. Динамическое подключение.(*.dll(win) *.so(lin))
+ Строится отдельньо от программы в отдельном проекте
+ Каждое приложение обращается
-- Сложность подключения
- Бинарная несовместимость
3.1 Статическое привязывание динамической библиотеки(код расположен в dll)
dll + exe (во время работы)
lib (таблица функций)
exe + lib (во время сборки)
- exe привязан к библиотеке(краш при отсутствии)
загрузка dll при старте
3.2 Динамическое привязывание динамической библиотеки
+ exe грузит dll в любой момент самостоятельно
+ возможность проверки перед загрузкой
- сложное подключение
Основня функция шифрования EncryptUpdate шифрует данные порциями через буффер заданной длины - это значит, что нужно сам файл считывать порциями в цикле от начала и до конца, размер буффера не кратен размеру файла, это зачит, что последний буффер будет заполнен не полностью, чтоб корректно обработать последний неполный буффер нужно вызвать вторую функцию для шифрофания EncryptFinal_ex. функции openssl рассчитаны на C, а не на C++, для максимальной перенасимости и совместимости, это значит, что данные передаются в функции не в виде строк (string) , не в виде других объектов, а в виде массивов char, а массивы char - это и есть буффер. должны быть беззнаковыми unsigned, перед началом шифрования нужно настроить структуру EVP_CIPHER_CTX с помощью функции EVP_EncryptInit_ex


*/
