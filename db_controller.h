#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>

class db_controller : public QObject
{
    Q_OBJECT
public:
    explicit db_controller(QObject *parent = nullptr);
    QSqlDatabase db;
    void sendQuieries(void);
    ~db_controller();
signals:

public slots:
};

#endif // DB_CONTROLLER_H
